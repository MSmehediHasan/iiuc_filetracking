<?php
session_start();
if (isset($_SESSION['user_id']))
    echo " ";
else
    header("location:../index.php");


if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $getid = $_GET['id'];
    include('../db_connect.php');

    if (!empty($getid)) {
        $query = mysqli_query($con, "SELECT * FROM file WHERE file_id='$getid'");
        $row = mysqli_fetch_assoc($query);
    }
}


$id = $name = $email = $password = '';
$idErr = $nameErr = $emailErr = $passwordErr = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    $user_id = $_POST['user_id'];
    $file_name = $_POST['file_name'];
    $dept = $_POST['dept'];
    $comment = $_POST['comment'];

    $file_id = $_POST['file_id'];

    $row['user_id'] = $user_id;
    $row['file_name'] = $file_name;

    //set default time zone
    date_default_timezone_set("Asia/Dhaka");

    //get current time
    $time=date("h:i A");

    //get current date
    $date=date("d M, Y");


    $status = "OK";
    $msg = "";

    if (empty($user_id)) { // checking your name
        $idErr = "* Please Enter Student ID.<BR>";
        $status = "NOTOK";
    }


    if (empty($file_name)) { // checking your name
        $nameErr = "* Please Enter File Name.<BR>";
        $status = "NOTOK";
    }




    if ($status == "OK") {

        include('../db_connect.php');


       $sql= "INSERT INTO file_status (user_id,file_name,comments,file_id,current_dept,time,date) VALUE ('$user_id','$file_name','$comment','$file_id','$dept','$time','$date') ";
        if (mysqli_query($con, $sql)) {
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal("SUCCESS!","Updated Sucessfully!","success");';
            echo '}, 500);</script>';


            echo '<script type="text/javascript">';
            echo "setTimeout(function () { window.open('all_file.php','_self')";
            echo '}, 1500);</script>';


        } else {// display the error message
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal("ERROR!","Something Wrong!","error");';
            echo '}, 500);</script>';

//  echo "<script>window.open('view_student.php','_self')</script>";
        }

    }
}

?>


<!doctype html>
<html lang="en">
<head>

    <style>
        .error {
            font-family: sans-serif;
            font-style: italic;
            color: #FF0000;
        }


        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript,
        if it's not present, don't show loader */
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
        }

        #my_button {
            display: inline-block;
            width: 150px;
            height: 50px;
            margin: 2px;
        }

    </style>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function () {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");

        });
    </script>

    <link rel="stylesheet" href="assets/swal2/sweetalert2.min.css" type="text/css"/>

    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Update</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>

</head>
<body>

<div class="se-pre-con"></div> <!--For preloader-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/swal2/sweetalert2.min.js"></script>


<div class="wrapper">
    <div class="sidebar" data-color="blue" data-image="assets/img/sidebar-5.jpg">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Admin Panel
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>


                <li>
                    <a href="students.php">
                        <i class="pe-7s-plus"></i>
                        <p>Students</p>
                    </a>

                <li>
                    <a href="all_admin.php">
                        <i class="pe-7s-plus"></i>
                        <p>All Admin</p>
                    </a>
                </li>


                <li>
                    <a href="view_department.php">
                        <i class="pe-7s-plus"></i>
                        <p>Department</p>
                    </a>
                </li>

                <li class="active">
                    <a href="all_file.php">
                        <i class="pe-7s-plus"></i>
                        <p>All File</p>
                    </a>
                </li>



                <li>
                    <a href="logout.php">
                        <i class="pe-7s-power"></i>
                        <p>Logout</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">File Update</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">

                    </ul>
                </div>
            </div>
        </nav>
        <br>

        <form role="form" id="student_info_form" name="form3" method="post" action="edit_file.php">


            <div class="form-group">
                <div class="col-xs-10">
                    <br>
                    <div class="input-group ">

                        <input type="hidden" class="form-control" name="file_id" value="<?php echo $row['file_id']; ?>"
                               id="student_id" readonly>


                    </div>
                    <span class="error"> <?php echo $idErr; ?></span>
                    <br>


                    <span>Your file name</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" name="file_name" id="name"
                               value="<?php echo $row['file_name']; ?>" readonly>

                    </div>

                    <span class="error"> <?php echo $nameErr; ?></span>
                    <br>


                    <span>Student ID</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" name="user_id" class="form-control" id="email"
                               value="<?php echo $row['user_id']; ?>" readonly>

                    </div>
                    <span class="error"> <?php echo $emailErr; ?></span>
                    <br>

                    <span>File Notes</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="text" name="note" class="form-control" id="latitude"
                               value="<?php echo $row['note']; ?>" readonly>

                    </div>


                    <span class="error"> <?php echo $passwordErr; ?></span>


                    <br>

                    <div class="form-group">
                        <label for="exampleInputCategory">Change/Forward Department</label>
                        <select class="form-control" name="dept" id="exampleInputCategory">
                            <?php echo "<option value='" . $row['department'] . "'>" . $row['department'] . "</option>"; ?>

                            <?php
                            include('db_connect.php');
                            $result = mysqli_query($con, "SELECT * FROM department");
                            while ($row2 = mysqli_fetch_array($result)) {
                                echo "<option value='" . $row2['dept_name'] . "'>" . $row2['dept_name'] . "</option>";

                            }
                            echo "</select>";

                            ?>
                    </div>


                    <br>


                    <span>Write your comments</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <textarea type="text" rows="4" class="form-control" name="comment" placeholder="Write your comment here"></textarea>

                    </div>

                    <br>

                    <button type="submit" id="btn_submit" class="btn btn-info btn-fill pull-right">Submit</button>
                    <div class="clearfix"></div>
                </div>
            </div>

        </form>


        <script type="text/javascript">

            $(document).on('click', '#btn_submit', function (e) {
                e.preventDefault();
                swal({
                    title: "ARE YOU SURE ?",
                    text: "Want to update information ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "YES",
                    cancelButtonText: "NO",
                    closeOnConfirm: false,
                    closeOnCancel: false

                }).then(function (e) {
                    $('#student_info_form').submit();
                });
            });


        </script>


    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>


</html>
