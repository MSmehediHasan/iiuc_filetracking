<?php
session_start();


$submit = "";

$status = "OK";
$msg = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $user_id = $_POST['user_id'];
    $user_type = $_POST['user_type'];
    $password = $_POST['password'];

    // echo $user_type;


    if (empty($user_id)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please Enter Your User ID. </font></center>";


        $status = "NOTOK";

    }


    if (empty($user_type)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please select user type. </font></center>";


        $status = "NOTOK";

    }


    if (empty($password)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please Enter Your password.";

        $status = "NOTOK";
    }

    if ($status == "OK") {

        include('db_connect.php');


        $result1 = mysqli_query($con, "SELECT * FROM admin WHERE user_id = '$user_id' AND password ='$password' ");
        $result2 = mysqli_query($con, "SELECT * FROM student WHERE user_id='$user_id' AND password ='$password' ");

        $count = mysqli_num_rows($result1); //admin

        $count2 = mysqli_num_rows($result2); //student


        if ($count == 1 and $user_type == 'admin') {

            $row = mysqli_fetch_array($result1);

            $_SESSION['user_id'] = $row['user_id'];
            $_SESSION['department'] = $row['department'];

            header("location:admin/dashboard.php");

        } else if ($count2 == 1 and $user_type == 'student') {

            $row = mysqli_fetch_array($result2);

            $_SESSION['user_id'] = $row['user_id'];


            header("location:student/dashboard.php");
        } else {


            $msg = "<center><font  size='4px' face='Verdana' size='1' color='red'>Wrong User ID or Password !!!.";

            echo "<div class='alert-danger' align='center'>" . $msg . "</div";

        }
    } else {
        echo "<div class='alert-danger' align='center'>" . $msg . "</div";
    }

}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>File Tracking System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form p-l-55 p-r-55 p-t-178" action="index.php" method="post">

                    <span class="login100-form-title">
						IIUC File Tracking System<br>Login
					</span>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter user id">
                    <input class="input100" type="text" name="user_id" placeholder="User ID">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Please enter password">
                    <input class="input100" type="password" name="password" placeholder="Password">
                    <span class="focus-input100"></span>
                </div>


                <br>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Select User Type</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="user_type" required>
                        <option disabled value="">Select User Type</option>
                        <option value="admin">Admin</option>
                        <option value="student">Student</option>
                    </select>
                </div>


                <br>


                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Login
                    </button>
                </div>

                <div class="flex-col-c p-t-170 p-b-40">
						<span class="txt1 p-b-9">
							Don’t have an account?
						</span>

                    <a href="signup.php" class="txt3">
                        Sign up now
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>


<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>