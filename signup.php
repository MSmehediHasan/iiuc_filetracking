<?php
session_start();
//if (isset($_SESSION['user_id'])) {
//    header("location:admin/dashboard.php");
//
//} else {
//    echo "";
//}


$submit = "";

$status = "OK";
$msg = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name = $_POST['name'];
    $email = $_POST['email'];
    $user_id = $_POST['user_id'];
    $cell = $_POST['cell'];
    $department = $_POST['department'];
    $gender = $_POST['gender'];
    $password = $_POST['password'];


    if (empty($user_id)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please Enter Your User ID. </font></center>";


        $status = "NOTOK";

    }


    if (empty($email)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please enter email. </font></center>";


        $status = "NOTOK";

    }

    if (empty($cell)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please enter valid cell. </font></center>";


        $status = "NOTOK";

    }


    if (empty($password)) {
        $msg .= "<center><font  size='4px' face='Verdana' size='1' color='red'>Please Enter Your password.";

        $status = "NOTOK";
    }

    if ($status == "OK") {

        include('db_connect.php');

        $result = mysqli_query($con, "SELECT * FROM student where user_id ='$user_id'");
        $num_rows = mysqli_num_rows($result);


        if ($num_rows > 0) {

            echo "<script>alert('User already exists!')</script>";


        } else {
            if (mysqli_query($con, "INSERT INTO student (`name`,`gender`,`email`,`user_id`,`department`,`password`) VALUE ('$name','$gender','$email','$user_id','$department','$password')")) {

                echo "<script>alert('Registration Successful!')</script>";

                echo "<script>window.open('index.php','_self')</script>";


            } else {// display the error message
                echo "<script>alert('Something went wrong. Please try again!')</script>";
            }

        }
    } else {
        echo "<div class='alert-danger' align='center'>" . $msg . "</div";
    }

}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>File Tracking System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form p-l-55 p-r-55 p-t-178" action="signup.php" method="post">

                    <span class="login100-form-title">
						IIUC File Tracking System<br>Student Registration
					</span>

                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter your name">
                    <input class="input100" type="text" name="name" placeholder="Student Name">
                    <span class="focus-input100"></span>
                </div>


                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter email">
                    <input class="input100" type="text" name="email" placeholder="Email">
                    <span class="focus-input100"></span>
                </div>


                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter phone number">
                    <input class="input100" type="text" name="cell" placeholder="Phone Number">
                    <span class="focus-input100"></span>
                </div>


                <div class="wrap-input100 validate-input m-b-16" data-validate="Please enter user id">
                    <input class="input100" type="text" name="user_id" placeholder="User ID/Student ID">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Please enter password">
                    <input class="input100" type="password" name="password" placeholder="Password">
                    <span class="focus-input100"></span>
                </div>


                <br>


                <div class="form-group">
                    <label for="exampleFormControlSelect1">Select Gender</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="gender" required>
                        <option disabled value="">Select Gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
                <br>

                <div class="form-group">
                    <label for="exampleFormControlSelect1">Select Department</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="department" required>
                        <option disabled value="">Select Department</option>
                        <option value="CSE">CSE</option>
                        <option value="EEE">EEE</option>
                        <option value="ETE">ETE</option>
                        <option value="BBA">BBA</option>
                        <option value="ELL">ELL</option>
                    </select>
                </div>
                <br>


                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Sign Up
                    </button>
                </div>

                <div class="flex-col-c p-t-170 p-b-40">


                    <a href="index.php" class="txt3">
                        Back TO Login
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>


<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>