<?php
session_start();
if (isset($_SESSION['user_id']))
    echo " ";
else
    header("location:../index.php");

$file_name=$dept=$note = '';
$nameErr=$deptErr =$noteErr= '';
if ((isset($_POST['file_name'])) != "" && $_SERVER["REQUEST_METHOD"] == "POST") {

    //set default time zone
    date_default_timezone_set("Asia/Dhaka");

    //get current time
    $time=date("h:i A");

    //get current date
    $date=date("d M, Y");
    $file_status='PENDING';


    $file_name = $_POST['file_name'];

    $user_id = $_POST['id'];
    $dept = $_POST['department'];
    $note= $_POST['note'];

    //get file name
    $filename = basename($_FILES['uploadedfile']['name']);


    $status = "OK";
    $msg = "";

    if (empty($file_name)) { // checking your name
        $nameErr = "* Please Enter File Name!.<BR>";
        $status = "NOTOK";
    }


    if ($status == "OK") {

        include('../db_connect.php');


        $result = mysqli_query($con, "SELECT * FROM file WHERE file_name='$file_name' AND user_id='$user_id'");
        $num_rows = mysqli_num_rows($result);


        if ($num_rows > 0) {
            echo '<script type="text/javascript">';
            echo 'setTimeout(function () { swal("ERROR!","Same file already exists!","error");';
            echo '}, 500);</script>';
        } else {


            if (empty($filename)) {
                $newfilename = 'image_placeholder.png';
            } else {

                $sms_code = time();
                //generate random file name
                $temp = explode(".", $_FILES["uploadedfile"]["name"]);
                $newfilename = $sms_code . '.' . end($temp);
                move_uploaded_file($_FILES["uploadedfile"]["tmp_name"], "../files/" . $newfilename);
            }




            if (mysqli_query($con, "INSERT INTO file (`file_name`,`user_id`,`department`,`note`,`date`,`time`,`status`,`doc_name`) VALUE ('$file_name','$user_id','$dept','$note','$date','$time','$file_status','$newfilename')")) {
                echo '<script type="text/javascript">';
                echo 'setTimeout(function () { swal("File Successfully Added!","Done!","success");';
                echo '}, 500);</script>';


                echo '<script type="text/javascript">';
                echo "setTimeout(function () { window.open('view_file.php','_self')";
                echo '}, 1500);</script>';

            } else {// display the error message
                echo '<script type="text/javascript">';
                echo 'setTimeout(function () { swal("ERROR!","Something Wrong!","error");';
                echo '}, 500);</script>';
            }

        }
    }
}
?>


<!doctype html>
<html lang="en">
<head>

    <style>
        .error {
            font-family: sans-serif;
            font-style: italic;
            color: #FF0000;
        }

        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript,
        if it's not present, don't show loader */
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
        }

        #my_button {
            display: inline-block;
            width: 150px;
            height: 50px;
            margin: 2px;
        }

    </style>


    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function () {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

    <link rel="stylesheet" href="assets/swal2/sweetalert2.min.css" type="text/css"/>

    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Add File</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet"/>

</head>
<body>

<!-- <div class="se-pre-con"></div> For preloader-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/swal2/sweetalert2.min.js"></script>


<div class="wrapper">
    <div class="sidebar" data-color="green" data-image="assets/img/sidebar-2.jpg">


        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Student Panel
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="dashboard.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>



                <li>
                    <a href="view_file.php">
                        <i class="pe-7s-plus"></i>
                        <p>View File</p>
                    </a>
                </li>


                <li  class="active">
                    <a href="add_file.php">
                        <i class="pe-7s-plus"></i>
                        <p>Add File</p>
                    </a>
                </li>


                <li>
                    <a href="logout.php">
                        <i class="pe-7s-power"></i>
                        <p>Logout</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Add File</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">

                    </ul>
                </div>
            </div>
        </nav>
        <br>

        <form enctype="multipart/form-data" role="form" id="student_info_form" name="form3" method="post" action="add_file.php">


            <h3 align="center">Add File</h3>


            <div class="form-group">
                <div class="col-xs-10">
                    <br>

                    <span>Your ID</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" name="id" value="<?php echo $_SESSION['user_id'];?>" readonly>

                    </div>
                    <br>

                    <span>Your file name</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" class="form-control" name="file_name" placeholder="Enter File Name" required>

                    </div>

                    <span class="error"> <?php echo $nameErr; ?></span>
                    <br>

                    <span>Select Department</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <select class="form-control" name="department" required >

                            <option value="" Selected Disabled>Select Department</option>

                            <?php
                            include('../db_connect.php');

                            $result = mysqli_query($con, "SELECT dept_name FROM department");
                            //sort($result);
                            while ($row = mysqli_fetch_array($result)) {
                                echo "<option value='" . $row['dept_name'] . "'>" . $row['dept_name'] . "</option>";

                            }
                            echo "</select>";

                            ?>


                    </div>

                    <span class="error"> <?php echo $deptErr; ?></span>
                    <br>


                    <span>Add your note</span>
                    <div class="input-group ">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <textarea type="text" rows="4" class="form-control" name="note" placeholder="Write your note here"></textarea>

                    </div>


                    <br>

                    <div class="form-group ">
                        <label for="exampleInputImage">Choose file to upload</label>


                        <div class="custom-file">
                            <input type="hidden" class="custom-file-input" id="fileupload" name="MAX_FILE_SIZE"
                                   value="9000000"/>

                            Choose a  file to upload:<br> <input class="btn btn-default" name="uploadedfile"
                                                                        type="file"/>
                            (Select file to upload)<br/>
                        </div>
                    </div>


                    <br>
                    <button type="submit" id="btn_submit" class="btn btn-info btn-fill pull-right">Submit</button>
                    <div class="clearfix"></div>

                    <br>
                    <br>
                </div>
            </div>

        </form>


        <script type="text/javascript">

            $(document).on('click', '#btn_submit', function (e) {
                e.preventDefault();
                swal({
                    title: "ARE YOU SURE ?",
                    text: "Want to add new file?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "YES",
                    cancelButtonText: "NO",
                    closeOnConfirm: false,
                    closeOnCancel: false

                }).then(function (e) {
                    $('#student_info_form').submit();
                });
            });


        </script>


    </div>
</div>


</body>



<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>


</html>
